package com.labomobile.listecourse.common.request;

/**
 * Created by nicolas.pattyn on 06/11/2017.
 */

public interface ResultCallback<T> {
    void onRequestFinished(String obj);
    void onChangeFinished(String obj);
    void onDataChange(T obj);
    void onError(String error);
}
