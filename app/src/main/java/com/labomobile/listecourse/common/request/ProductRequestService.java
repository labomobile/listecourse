package com.labomobile.listecourse.common.request;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.labomobile.listecourse.common.model.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicolas.pattyn on 06/11/2017.
 */

public class ProductRequestService {

    //Singleton, permettant d'accéder à la même instance partout
    private static ProductRequestService SINGLETON;

    // Objet de votre bdd
    private FirebaseDatabase database;

    // Reférence à votre objet/array
    private DatabaseReference dbProductRef;

    // Référence en "dur" pour éviter de réécrire "products" partout
    private String productsRef = "products";

    // M2thode de mise en place de notre singleton
    private static final Object __synchonizedObject = new Object();

    public static ProductRequestService getInstance() {
        if(SINGLETON == null) {
            synchronized (__synchonizedObject) {
                if(SINGLETON == null) {
                    SINGLETON = new ProductRequestService();
                }
            }
        }
        return SINGLETON;
    }

    // Constructeur
    private ProductRequestService() {
        database = FirebaseDatabase.getInstance();

        dbProductRef = database.getReference(productsRef);
    }

    // Ajout de produit à la liste
    public void AddProduct(Product product, final ResultCallback callback){
        dbProductRef.push().setValue(product).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                callback.onRequestFinished("");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError(e.getMessage());
            }
        });
    }

    // Maj d'un produit
    public void UpdateProduct(Product product, final ResultCallback callback){
        Product p = new Product();
        p.setName(product.getName());
        p.setChecked(product.isChecked());
        dbProductRef.child(product.getRef()).setValue(p).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                callback.onChangeFinished("");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError(e.getMessage());
            }
        });
    }

    // On écoute notre référence pour savoir si il y a des changements
    public void GetProducts(final ResultCallback<ArrayList<Product>> callback){
        // On commence à écouter, Firebase ouvre un nouveau thread
        dbProductRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Firebase nous renvois un dictionnaire (hashMap) de clé/valeur
                // On doit donc récupérer notre objet, puis la liste de produits
                GenericTypeIndicator<HashMap<String, Product>> objectsGTypeInd = new GenericTypeIndicator<HashMap<String, Product>>() {};

                //On récupère notre hashmap
                Map<String, Product> objectHashMap = dataSnapshot.getValue(objectsGTypeInd);
                ArrayList<Product> objectArrayList = new ArrayList<Product>();
                if(objectHashMap != null){
                    // On récup la valeur de notre hashmap
                    objectArrayList.addAll(objectHashMap.values());
                    int i = 0;
                    // On parcours notre hashmap, afin de récupérer chaques références des produits, utilisé pour faire une maj ou le delete
                    for ( String key : objectHashMap.keySet() ) {
                        objectArrayList.get(i).setRef(key);
                        i++;
                    }
                }
                // On appelle notre callback
                callback.onDataChange(objectArrayList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Error Firebase", databaseError.getMessage());
                callback.onError(databaseError.getMessage());
            }
        });
    }

    // On supprime un produit, grace à sa référence
    public void RemoveProduct(String ref){
        dbProductRef.child(ref).removeValue();
    }
}
