package com.labomobile.listecourse.common.model;

/**
 * Created by nicolas.pattyn on 06/11/2017.
 */

public class Product {
    private String name;
    private String ref;
    private boolean checked;

    public Product() {
    }

    public Product(String name, boolean checked) {
        this.name = name;
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if(checked != product.checked) return false;
        if(name != null ? !name.equals(product.name) : product.name != null) return false;
        return ref != null ? ref.equals(product.ref) : product.ref == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (ref != null ? ref.hashCode() : 0);
        result = 31 * result + (checked ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", ref='" + ref + '\'' +
                ", checked=" + checked +
                '}';
    }
}
