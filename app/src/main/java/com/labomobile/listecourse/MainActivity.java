package com.labomobile.listecourse;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.swipe.util.Attributes;
import com.labomobile.listecourse.common.model.Product;
import com.labomobile.listecourse.common.request.ProductRequestService;
import com.labomobile.listecourse.common.request.ResultCallback;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ResultCallback<ArrayList<Product>>, ProductListAdapter.DeleteUpdateProductCallback{

    private EditText editText;
    private Button button;
    private ProductListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ProductRequestService.getInstance().GetProducts(this);
    }

    /**
     * On initialise nos composants
     */
    private void init(){
        editText = findViewById(R.id.editText);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        button = findViewById(R.id.button);
        button.setOnClickListener(this);
        adapter = new ProductListAdapter(this);
        adapter.setMode(Attributes.Mode.Single);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button:
                addProduct();
                break;
        }
    }

    /**
     * Appelé afin d'ajouter un produit à la liste
     */
    private void addProduct(){
        if(editText.getText() != null && !editText.getText().equals("")){
            // On rend le bouton non cliquable, afin de ne pas rajouter plusieurs produit durant la requête
            button.setClickable(false);
            ProductRequestService.getInstance()
                    .AddProduct(new Product(editText.getText().toString(), false), this);
        }else{
            Toast.makeText(this, R.string.add_product, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestFinished(String obj) {
        // On vide l'editText et on redonne la fonctionnalité au bouton
        editText.setText("");
        button.setClickable(true);
    }

    @Override
    public void onChangeFinished(String obj) {

    }

    @Override
    public void onDataChange(ArrayList<Product> products) {
        // La liste a été modifié, on met à jour la recyclerList
        adapter.fillList(products);
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void deleteProduct(String ref) {
        if(ref != null && !ref.equals("")){
            ProductRequestService.getInstance()
                    .RemoveProduct(ref);
        }
    }

    @Override
    public void updateProduct(Product p) {
        if(p != null){
            ProductRequestService.getInstance()
                    .UpdateProduct(p, this);
        }
    }
}
