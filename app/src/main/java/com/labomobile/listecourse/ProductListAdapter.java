package com.labomobile.listecourse;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.labomobile.listecourse.common.model.Product;

import java.util.ArrayList;

/**
 * Created by nicolas.pattyn on 06/11/2017.
 */

public class ProductListAdapter extends RecyclerSwipeAdapter<ProductListAdapter.ViewHolder> {

    private ArrayList<Product> products = new ArrayList<>();
    private DeleteUpdateProductCallback callback;

    // L'interface DeleteUpdateProductCallback permet de communiquer entre l'activité et l'adapter
    public ProductListAdapter(DeleteUpdateProductCallback callback) {
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false));
    }

    // On reçoit de nouvelles données, on les affiche
    public void fillList(ArrayList<Product> list){
        products.clear();
        products.addAll(list);
        notifyDataSetChanged();
    }

    // C'est plus propre de remplir une cellule directement dans l'holder
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.fillView(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    // Méthode lié au swipe
    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView productName;
        public CheckBox productCheck;
        private View delete;
        private SwipeLayout swipeLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            productName = itemView.findViewById(R.id.productName);
            productCheck = itemView.findViewById(R.id.productChecked);
            delete = itemView.findViewById(R.id.delete_exo);
            swipeLayout = itemView.findViewById(R.id.swipe);

            // Initialisation du swipe
            swipeLayout.setSwipeEnabled(true);
            swipeLayout.setLeftSwipeEnabled(true);
            swipeLayout.setRightSwipeEnabled(false);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
            swipeLayout.addDrag(SwipeLayout.DragEdge.Left, delete);

            delete.setOnClickListener(this);

            productCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    products.get(getPosition()).setChecked(b);
                    callback.updateProduct(products.get(getPosition()));
                }
            });
        }

        // Méthode utilisé pour remplir la cellule
        public void fillView(Product p){
            productName.setText(p.getName());
            productCheck.setChecked(p.isChecked());

            // On bind la vue au manager de swipe afin qu'il ne perde pas son état (ouvert/fermé)
            mItemManger.bindView(itemView, getPosition());

            mItemManger.updateConvertView(itemView, getPosition());
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.delete_exo:
                    //On ferme tous les swipes
                    closeAllItems();
                    callback.deleteProduct(products.get(getPosition()).getRef());
                    break;
            }
        }
    }

    // Interface pour communiquer avec l'activité
    public interface DeleteUpdateProductCallback{
        void deleteProduct(String ref);
        void updateProduct(Product p);
    }

}
